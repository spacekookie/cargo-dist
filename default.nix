with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rust";
  buildInputs = with pkgs; [
    rustup clangStdenv
  ];
  # shellHook = ''
  #   set -x target/debug $PATH
  # '';
}
