mod config;

use config::Config;
use std::{fs::File, io::Read};

fn main() {
    let mut f = File::open("./dist.toml").expect("No `dist.toml` found!");
    let mut toml = String::new();
    f.read_to_string(&mut toml).unwrap();

    let cfg = Config::load(toml);
}
