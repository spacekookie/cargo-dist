use std::collections::BTreeMap;

/// Encode a distributable source origins
pub(crate) enum Source {
    /// A file available in the source tree
    File(String),
    /// An artifact that's built with cargo
    Dependent(String),
}

/// Distributable artifact metadata
pub(crate) struct Artifact {
    source: Source,
    target: String,
}

/// Wrap `dist.toml` settings to apply 
pub(crate) struct Config {
    /// Distribution prefix
    prefix: String,
    /// Available path identifiers
    paths: BTreeMap<String, String>,
    /// Set of artifacts to be distributed
    artifacts: Vec<Artifact>
}

impl Config {
    /// Decode a provided `toml` string into a config
    pub(crate) fn load(file: String) -> Self {
        unimplemented!()
    }
}
